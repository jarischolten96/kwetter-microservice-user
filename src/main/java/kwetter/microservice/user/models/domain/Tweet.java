package kwetter.microservice.user.models.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Tweet {
    private UUID id;
    private Account user;
    private String text;
    private List<Account> likes;
    private Date date;
}
