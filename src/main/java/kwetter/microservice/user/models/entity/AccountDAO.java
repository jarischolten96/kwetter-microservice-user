package kwetter.microservice.user.models.entity;

import kwetter.microservice.user.models.entity.converter.UUIDConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class AccountDAO {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;
    private String email;
    private String userName;
    private String password;
    private String fullName;
    private String bio;
    @Column(name = "follower_ids")
    @Convert(converter = UUIDConverter.class)
    private List<UUID> followers;
    @Column(name = "following_ids")
    @Convert(converter = UUIDConverter.class)
    private List<UUID> following;
    @Column(name = "tweet_ids")
    @Convert(converter = UUIDConverter.class)
    private List<UUID> tweets;
}
