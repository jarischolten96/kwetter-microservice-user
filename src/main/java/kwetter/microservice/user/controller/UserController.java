package kwetter.microservice.user.controller;

import kwetter.microservice.user.mapper.ObjectMapper;
import kwetter.microservice.user.models.domain.Account;
import kwetter.microservice.user.models.domain.UserCredentials;
import kwetter.microservice.user.models.entity.AccountDAO;
import kwetter.microservice.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final ObjectMapper objectMapper;
    private final UserService userService;

    @PostMapping("/login")
    public ResponseEntity<Account> login(@RequestBody UserCredentials credentials)
    {
        Account account = userService.getAccountByEmail(credentials.getEmail());
        return ResponseEntity.ok(account);
    }

    @PostMapping("/authenticate")
    public ResponseEntity authenticate(@RequestBody UserCredentials credentials) throws Exception
    {
        userService.authenticate(credentials);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/register")
    public ResponseEntity<Account> register(@RequestBody Account account)
    {
        AccountDAO returnedAccount = userService.register(objectMapper.toAccountDAO(account));
        return ResponseEntity.ok(objectMapper.toAccount(returnedAccount));
    }

    @GetMapping("getUserById/{id}")
    public ResponseEntity<Account> getUserById(@PathVariable("id") String id)
    {
        return ResponseEntity.ok(userService.getUserById(UUID.fromString(id)));
    }
}
