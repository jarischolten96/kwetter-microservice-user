package kwetter.microservice.user.service;

import kwetter.microservice.user.custom_exceptions.CredentialsNotOkayException;
import kwetter.microservice.user.mapper.ObjectMapper;
import kwetter.microservice.user.models.domain.Account;
import kwetter.microservice.user.models.domain.UserCredentials;
import kwetter.microservice.user.models.entity.AccountDAO;
import kwetter.microservice.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;


import java.util.UUID;

@Service
@AllArgsConstructor
public class UserService {
    private final ObjectMapper objectMapper;
    private final UserRepository userRepository;

    public Account getAccountByEmail(String email){
        return objectMapper.toAccount(userRepository.findByEmail(email).orElse(null));
    }

    public Account getUserById(UUID id){
        return objectMapper.toAccount(userRepository.findById(id).orElse(null));
    }

    public void authenticate(UserCredentials credentials) throws CredentialsNotOkayException{
        Account acc = this.getAccountByEmail(credentials.getEmail());
        if (acc == null) throw new CredentialsNotOkayException();

        if(!BCrypt.checkpw(credentials.getPassword(), acc.getPassword())){
            throw new CredentialsNotOkayException();
        }
    }

    public AccountDAO register(AccountDAO account){
        String hashedPassword = BCrypt.hashpw(account.getPassword(), BCrypt.gensalt(10));
        account.setPassword(hashedPassword);
        return userRepository.save(account);
    }
}
