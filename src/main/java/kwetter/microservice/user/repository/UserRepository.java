package kwetter.microservice.user.repository;

import kwetter.microservice.user.models.entity.AccountDAO;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends CrudRepository<AccountDAO, UUID> {
    Optional<AccountDAO> findByEmail(final String email);
}
