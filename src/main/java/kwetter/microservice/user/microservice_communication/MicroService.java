package kwetter.microservice.user.microservice_communication;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import kwetter.microservice.user.service_response_exception.ServiceBadRequestException;
import kwetter.microservice.user.service_response_exception.ServiceConflictException;
import kwetter.microservice.user.service_response_exception.ServiceNotFoundException;
import kwetter.microservice.user.service_response_exception.ServiceUnhandledException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.List;

public abstract class MicroService //TODO: event bus
{
    protected static final String HTTP = "http";

    protected static final String BOOKING_SERVICE = "tml-bookings";
    protected static final String PLANNING_SERVICE = "tml-plannings";
    protected static final String EVENT_SERVICE = "tml-events";
    protected static final String FLIGHT_SERVICE = "tml-flights";
    protected static final String PAYMENT_SERVICE = "tml-payments";
    protected static final int BOOKING_PORT = 80;
    protected static final int PLANNING_PORT = 80;
    protected static final int EVENT_PORT = 80;
    protected static final int FLIGHT_PORT = 80;
    protected static final int PAYMENT_PORT = 80;

    private static final String CONTENT_TYPE  = "content-type";
    private static final String APPLICATION_JSON = "application/json";

    private ObjectMapper jacksonObjectMapper = new ObjectMapper();

    protected <T> T getItem(URIBuilder builder, Class<T> expectedReturnType) throws URISyntaxException, IOException {
        String jsonResponse = getJsonResponse(builder);
        return jacksonObjectMapper.readValue(jsonResponse, expectedReturnType);
    }

    protected <T> List<T> getList(URIBuilder builder) throws URISyntaxException, IOException {
        String jsonResponse = getJsonResponse(builder);
        return jacksonObjectMapper.readValue(jsonResponse, new TypeReference<List<T>>(){});
    }

    private String getJsonResponse(URIBuilder builder) throws URISyntaxException, IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(builder.build());
        httpGet.addHeader(CONTENT_TYPE, APPLICATION_JSON);
        CloseableHttpResponse response = httpClient.execute(httpGet);

        if (response.getStatusLine().getStatusCode() != 200) {
            handleStatusCode(response.getStatusLine().getStatusCode());
        }

        String jsonResponse = EntityUtils.toString(response.getEntity());
        httpClient.close();

        return jsonResponse;
    }

    protected <T> void putItem(URIBuilder builder, T body) throws IOException, URISyntaxException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPut httpPut = new HttpPut(builder.build());
        httpPut.addHeader(CONTENT_TYPE, APPLICATION_JSON);
        httpPut.setEntity(new StringEntity(new Gson().toJson(body)));
        CloseableHttpResponse response = httpClient.execute(httpPut);

        if (response.getStatusLine().getStatusCode() != 200) {
            handleStatusCode(response.getStatusLine().getStatusCode());
        }

        httpClient.close();
    }

    protected <T, E> E putItemWithResponse(URIBuilder builder, T body, Type type) throws IOException, URISyntaxException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPut httpPut = new HttpPut(builder.build());
        httpPut.addHeader(CONTENT_TYPE, APPLICATION_JSON);
        httpPut.setEntity(new StringEntity(new Gson().toJson(body)));
        CloseableHttpResponse response = httpClient.execute(httpPut);

        if (response.getStatusLine().getStatusCode() != 200) {
            handleStatusCode(response.getStatusLine().getStatusCode());
        }
        String jsonResponse = EntityUtils.toString(response.getEntity());
        httpClient.close();
        return jacksonObjectMapper.readValue(jsonResponse, this.getType(type));
    }

    protected <T> void postItem(URIBuilder builder, T body) throws URISyntaxException, IOException {
        // Call postItemWithResponse (same function) and ignore the return value
        postItemWithResponse(builder, body, void.class);
    }

    protected <T, E> E postItemWithResponse(URIBuilder builder, T body, Type type) throws URISyntaxException, IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(builder.build());
        httpPost.addHeader(CONTENT_TYPE, APPLICATION_JSON);
        httpPost.setEntity(new StringEntity(new Gson().toJson(body)));
        CloseableHttpResponse response = httpClient.execute(httpPost);

        if (response.getStatusLine().getStatusCode() != 201) {
            handleStatusCode(response.getStatusLine().getStatusCode());
        }

        String jsonResponse = EntityUtils.toString(response.getEntity());
        httpClient.close();
        if (type.equals(Void.TYPE)) return null;
        return jacksonObjectMapper.readValue(jsonResponse, this.getType(type));
    }

    private <E> TypeReference<E> getType(Type type)
    {
        return new TypeReference<E>()
        {
            @Override
            public Type getType()
            {
                return type;
            }
        };
    }

    private void handleStatusCode(int statusCode) {
        switch (statusCode) {
            case 400:
                throw new ServiceBadRequestException();
            case 404:
                throw new ServiceNotFoundException();
            case 409:
                throw new ServiceConflictException();
            default:
                throw new ServiceUnhandledException();
        }
    }
}