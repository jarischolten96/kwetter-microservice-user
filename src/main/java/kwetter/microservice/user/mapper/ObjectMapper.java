package kwetter.microservice.user.mapper;


import kwetter.microservice.user.models.domain.Account;
import kwetter.microservice.user.models.entity.AccountDAO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class ObjectMapper {

    public AccountDAO toAccountDAO(Account account)
    {
        return AccountDAO.builder()
                .id(account.getId())
                .email(account.getEmail())
                .password(account.getPassword())
                .fullName(account.getFullName())
                .userName(account.getUserName())
                .bio(account.getBio())
                .followers(new ArrayList<>())
                .following(new ArrayList<>())
                .tweets(new ArrayList<>())
                .build();
    }

    public Account toAccount(AccountDAO accountDAO)
    {
        return accountDAO == null ? null : Account.builder()
                .id(accountDAO.getId())
                .email(accountDAO.getEmail())
                .password(accountDAO.getPassword())
                .fullName(accountDAO.getFullName())
                .userName(accountDAO.getUserName())
                .bio(accountDAO.getBio())
                .followers(new ArrayList<>())
                .following(new ArrayList<>())
                .tweets(new ArrayList<>())
                .build();
    }
}
